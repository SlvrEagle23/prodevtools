<?php
namespace Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Action
 *
 * @Table(name="action")
 * @Entity
 */
class Action extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=100) */
    protected $name;


}