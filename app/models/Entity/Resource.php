<?php
namespace Entity;

use \Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="resource")
 * @Entity
 * @HasLifecycleCallbacks
 */
class Resource extends \DF\Doctrine\Entity
{
	public function __construct()
	{
		$this->created_at = $this->updated_at = new \DateTime('NOW');
		$this->categories = new ArrayCollection;
	}

    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="type_id", type="integer") */
    protected $type_id;

    /** @Column(name="title", type="string", length=255, nullable=true) */
    protected $title;

    /** @Column(name="author", type="string", length=255, nullable=true) */
    protected $author;

    /** @Column(name="url", type="string", length=255, nullable=true) */
    protected $url;

    /** @Column(name="description", type="text", nullable=true) */
    protected $description;

    /** @Column(name="created_at", type="datetime") */
    protected $created_at;

    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;

    /**
     * @ManyToOne(targetEntity="ResourceType")
     * @JoinColumn(name="type_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $type;

    /**
     * @ManyToMany(targetEntity="ResourceCategory", inversedBy="resources")
     * @JoinTable(name="resource_has_category",
     *      joinColumns={@JoinColumn(name="resource_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $categories;
}