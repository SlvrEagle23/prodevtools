<?php
namespace Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="user")
 * @Entity
 */
class User extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->roles = new ArrayCollection;
        $this->logs = new ArrayCollection;
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="email", type="string", length=150, nullable=true) */
    protected $email;

    /** @Column(name="password", type="string", length=40, nullable=true) */
    protected $password;

    /** @Column(name="password_salt", type="string", length=40, nullable=true) */
    protected $password_salt;

    public function getPassword()
    {
        return '';
    }

    public function setPassword($password)
    {
        $this->password_salt = sha1(mt_rand());
        $this->password = hash_hmac('sha1', $password, $this->password_salt);
    }

    /** @Column(name="password_reset_code", type="string", length=5, nullable=true) */
    protected $password_reset_code;

    /** @Column(name="firstname", type="string", length=50, nullable=true) */
    protected $firstname;

    /** @Column(name="lastname", type="string", length=50, nullable=true) */
    protected $lastname;

    /** @Column(name="phone", type="string", length=20, nullable=true) */
    protected $phone;

    /**
     * @ManyToMany(targetEntity="Role", inversedBy="users")
     * @JoinTable(name="user_has_role",
     *      joinColumns={@JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $roles;

    /**
     * @OneToMany(targetEntity="ResourceLog", mappedBy="user")
     * @OrderBy({"id" = "DESC"})
     */
    protected $logs;

    public static function authenticate($email, $password)
    {
        $user = self::getRepository()->findOneBy(array('email' => $email));

        if ($user instanceof self)
        {
            $new_pw_hash = hash_hmac('sha1', $password, $user->password_salt);

            if (strcmp($new_pw_hash, $user->password) == 0)
                return $user;
            else
                return NULL;
        }

        return NULL;
    }
}