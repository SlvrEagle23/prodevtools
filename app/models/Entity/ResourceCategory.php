<?php
namespace Entity;

use \Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="resource_category")
 * @Entity
 */
class ResourceCategory extends \DF\Doctrine\Entity
{
	public function __construct()
	{
		$this->resources = new ArrayCollection;
	}

    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="name", type="string", length=255, nullable=true) */
    protected $name;

    /** @ManyToMany(targetEntity="Resource", mappedBy="categories") */
    protected $resources;

    public static function fetchSelect()
    {
        $items = array();
        $items_raw = self::fetchArray('name');

        foreach((array)$items_raw as $item)
            $items[$item['id']] = $item['name'];

        return $items;
    }
}