<?php
namespace Entity;

use \Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Table(name="resource_logs")
 * @Entity
 */
class ResourceLog extends \DF\Doctrine\Entity
{
	public function __construct()
	{
        $this->created_at = $this->updated_at = new \DateTime('NOW');
	}

    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="user_id", type="integer") */
    protected $user_id;

    /** @Column(name="resource_id", type="integer") */
    protected $resource_id;

    /** @Column(name="seconds", type="integer") */
    protected $seconds;

    /** @Column(name="created_at", type="datetime") */
    protected $created_at;

    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;

    /**
     * @ManyToOne(targetEntity="Resource")
     * @JoinColumn(name="resource_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $resource;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="logs")
     * @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;
}