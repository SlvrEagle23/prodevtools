<?php
/**
 * Application Settings
 */

$config = array(
	// Application name
	'name'				=> 'Professional Development Tools',
    'base_url'          => '//professionaldevelopmenttools.com',

	// Google analytics code for site
	'analytics_code'	=> 'UA-32840576-1',
    
    // DF Messenger mail settings
    'mail'				=> array(
        'templates'			=> DF_INCLUDE_BASE.'/messages',
        'from_addr'         => 'info@profesisonaldevelopmenttools.com',
        'from_name'         => 'Professional Development Tools',
        'bounce_addr'	    => 'noreply@professionaldevelopmenttools.com',
        'error_addr'        => 'buster@busterneece.com',
        'use_smtp'          => true,
    ),
	
	'phpSettings'		=> array(
		'display_startup_errors'		=> 1,
		'display_errors'				=> 1,
        'error_reporting'               => E_ALL & ~E_NOTICE & ~E_STRICT,
		'date'				=> array(
			'timezone'			=> 'America/Chicago',
		),
	),
		
	'bootstrap'			=> array(
		'path'				=> 'DF/Application/Bootstrap.php',
		'class'				=> '\DF\Application\Bootstrap',
	),
	
	'includePaths'		=> array(
		DF_INCLUDE_LIB,
        DF_INCLUDE_LIB.'/Doctrine',
    ),
	
	'pluginpaths'		=> array(
		'DF\Application\Resource\\' => 'DF/Application/Resource',
	),
    
    'autoload'          => array(
        'psr0'      => array(
            'DF'        => DF_INCLUDE_LIB,
            'PD'        => DF_INCLUDE_LIB,
            'Entity'    => DF_INCLUDE_MODELS,
        ),
        'psr4'      => array(
            '\\Proxy\\'     => DF_INCLUDE_TEMP.'/proxies',
        ),
	),
	
	'resources'			=> array(
		/* RESOURCES: Locale */
		'locale'			=> array(
			'default'			=> 'en_US',
		),
		
		/* RESOURCES: Front Controller */
		'frontController'	=> array(
            'disableOutputBuffering' => true,
			'throwerrors'		=> true,
			'moduleDirectory'	=> DF_INCLUDE_MODULES,
			'moduleControllerDirectoryName' => "controllers",
			'defaultModule'		=> "default",
			'defaultAction'		=> "index",
			'defaultControllerName' => "index",
		),
		
		/* RESOURCES: Doctrine ORM Layer */
		'doctrine'			=> array(
            'autoGenerateProxies' => (DF_APPLICATION_ENV == "development"),
            'proxyNamespace'    => 'Proxy',
            'proxyPath'         => DF_INCLUDE_TEMP.'/proxies',
            'modelPath'         => DF_INCLUDE_MODELS,
		),
        
		/* RESOURCES: Menu */
		'menu'				=> array(
			'enabled'			=> true,
		),
		
		/* RESOURCES: Layout */
		'layout'			=> array(
			'layout'			=> 'default',
			'layoutPath'		=> DF_INCLUDE_BASE.'/layouts',
			'commonTemplates'	=> DF_INCLUDE_BASE.'/common',
		),
	),
);

/**
 * Development mode changes.
 */

if (DF_APPLICATION_ENV != 'production')
{
    $config['phpSettings']['display_startup_errors'] = 1;
    $config['phpSettings']['display_errors'] = 1;

    // Update if your local configuration differs.
    unset($config['base_url']);
}

return $config;