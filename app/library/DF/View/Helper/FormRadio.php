<?php
namespace DF\View\Helper;

class FormRadio extends \Zend_View_Helper_FormRadio
{
    public function formRadio($name, $value = null, $attribs = null,
        $options = null, $listsep = "<br />\n")
    {
        $raw = parent::formRadio($name, $value, $attribs, $options);

        $group_class = ($attribs['inline']) ? 'inline' : 'block';
        return '<div class="radio-group '.$group_class.'">'.$raw.'</div>';
    }
}
