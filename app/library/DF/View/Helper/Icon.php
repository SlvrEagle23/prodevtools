<?php
namespace DF\View\Helper;
class Icon extends \Zend_View_Helper_Abstract
{
    /**
     * @return string The icon's image tag.
     */
    public function icon($icon)
    {
        if (is_array($icon))
            $icon = $icon['image'];

        if (substr($icon, 0, 5) == 'icon-')
            $icon = str_replace('icon-', 'fa-', $icon);

        if (substr($icon, 0, 3) == 'fa-')
            return '<i class="fa '.$icon.'"></i>';
        else
            return '';
    }
}