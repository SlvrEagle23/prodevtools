<?php
use \Entity\ResourceType;
use \Entity\ResourceType as Record;

class Admin_TypesController extends \DF\Controller\Action
{
    public function permissions()
    {
        return $this->acl->isAllowed('administer all');
    }

    public function indexAction()
    {
        $this->view->categories = Record::fetchAll();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $form = new \DF\Form($this->current_module_config->forms->resource_category);

        if ($id != 0)
        {
            $record = Record::find($id);
            $form->setDefaults($record->toArray(TRUE, TRUE));
        }

        if( !empty($_POST) && $form->isValid($_POST) )
        {
            $data = $form->getValues();

            if (!($record instanceof Record))
                $record = new Record;

            $record->fromArray($data);
            $record->save();

            $this->alert('<b>Resource category updated!</b>', 'green');

            $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
            return;
        }

        $this->view->headTitle('Add/Edit Resource Type');
        $this->renderForm($form);
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        $row = Record::find($id);

        if ($row instanceof Record)
            $row->delete();

        $this->alert('<b>Resource type removed.</b>', 'green');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
        return;
    }
}