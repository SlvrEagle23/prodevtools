<?php
use \Entity\Resource;
use \Entity\ResourceCategory;
use \Entity\ResourceType;

class Admin_ResourcesController extends \DF\Controller\Action
{
    public function permissions()
    {
		return $this->acl->isAllowed('administer all');
    }

    public function indexAction()
    {
        $query = $this->em->createQueryBuilder()
            ->select('r')
            ->from('Entity\Resource', 'r')
            ->orderBy('r.title', 'ASC');

        if ($this->_hasParam('q'))
        {
            $q = $this->_getParam('q');
            $this->view->q = $q;

            $query->where('r.title LIKE :q OR r.author LIKE :q OR r.description LIKE :q')->setParameter('q', '%'.$q.'%');
        }
        
        $this->view->pager = new \DF\Paginator\Doctrine($query, $this->_getParam('page', 1));
    }
    
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
		$form = new \DF\Form($this->current_module_config->forms->resource);

        if ($id != 0)
        {
            $record = Resource::find($id);
            $form->setDefaults($record->toArray(TRUE, TRUE));
        }
        
		if( !empty($_POST) && $form->isValid($_POST) )
		{
			$data = $form->getValues();

            if (!($record instanceof Resource))
                $record = new Resource;

			$record->fromArray($data);
			$record->save();
			
			$this->alert('<b>Resource updated!</b>', 'green');
			
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
            return;
		}

        $this->view->headTitle('Add/Edit Resource');
        $this->renderForm($form);
    }
    
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        $user = Resource::find($id);
		
		if ($user instanceof Resource)
            $user->delete();
        
        $this->alert('<b>Resource removed.</b>');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
        return;
    }
}