<?php
return array(
    'method'		=> 'post',
    'elements'		=> array(

        'name'	=> array('text', array(
            'label' => 'Category Name',
            'class' => 'half-width',
            'required' => true,
            'description' => 'Note: Adding the characters " - ", as in "Category - Subcategory" will place the new item as a subcategory underneath the prefix on listings.',
        )),

        'submit'		=> array('submit', array(
            'type'	=> 'submit',
            'label'	=> 'Save Changes',
            'helper' => 'formButton',
            'class' => 'ui-button',
        )),
    ),
);