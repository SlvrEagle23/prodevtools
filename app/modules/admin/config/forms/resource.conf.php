<?php


return array(	
	'method'		=> 'post',
	'elements'		=> array(

        'title'	=> array('text', array(
            'label' => 'Resource Title',
            'class' => 'full-width',
            'required' => true,
        )),
		
		'type_id' => array('select', array(
			'label' => 'Resource Type',
			'multiOptions' => \Entity\ResourceType::fetchSelect(),
			'required' => true,
        )),

        'categories' => array('multiCheckbox', array(
        	'label' => 'Categories',
        	'multiOptions' => \Entity\ResourceCategory::fetchSelect(),
        	'required' => true,
        )),

		'author' => array('text', array(
			'label' => 'Resource Author(s)',
			'class' => 'full-width',
			'required' => true,
		)),

		'url' => array('text', array(
			'label' => 'Resource URL',
			'description' => 'Remember to include http:// at the beginning of the URL.',
			'class' => 'full-width',
			'required' => true,
		)),

		'description' => array('textarea', array(
			'label' => 'Resource Description',
			'class' => 'full-width full-height',
		)),
		
		'submit'		=> array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Save Changes',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);