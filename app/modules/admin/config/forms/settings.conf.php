<?php
/**
 * Settings form.
 */

return array(
	/**
	 * Form Configuration
	 */
	'form' => array(
		'method'		=> 'post',
        'elements'          => array(
            'submit'		=> array('submit', array(
                'type'	=> 'submit',
                'label'	=> 'Save Changes',
                'helper' => 'formButton',
                'class' => 'ui-button',
            )),
        ),
	),
);