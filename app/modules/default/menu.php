<?php
return array(
	'default' => array(
		'home'		=> array(
			// Home page.
			'label'		=> 'Home',
			'module'	=> 'default',
			'controller' => 'index',
			'order'		=> -10,
			'pages'		=> array(),
		),
		
		'category' => array(
			'label' => 'Browse By Category',
			'module' => 'default',
			'controller' => 'resources',
			'action' => 'category',
		),
		'search' => array(
			'label' => 'Search Resources',
			'module' => 'default',
			'controller' => 'resources',
			'action' => 'search',
		),

		'report' => array(
			'label' => 'My Report',
			'module' => 'default',
			'controller' => 'report',
			'permission' => 'is logged in',
		),

        'resources' => array(
            'label' => 'Resources',
            'module' => 'default',
            'controller' => 'resources',
            'action' => 'index',

            'pages' => array(
                'resource_category' => array(
                    'module' => 'default',
                    'controller' => 'resources',
                    'action' => 'category',
                ),
                'resource_search' => array(
                    'module' => 'default',
                    'controller' => 'resources',
                    'action' => 'search',
                ),
                'resource_view' => array(
                    'module' => 'default',
                    'controller' => 'resources',
                    'action' => 'view',
                ),
            ),
        ),
	
		/*
		'account'	=> array(
			// Account
			'label'		=> 'My Account',
			'module'	=> 'default',
			'controller' => 'account',
			'order'     => -9,
			
			'pages'		=> array(
				'profile'		=> array(
					'label'			=> 'Edit Profile',
					'module'		=> 'default',
					'controller'	=> 'account',
					'action'		=> 'profile',
					'permission'	=> 'is logged in',
				),
				'login'			=> array(
					'label'			=> 'Log In',
					'module'		=> 'default',
					'controller'	=> 'account',
					'action'		=> 'login',
					'permission'	=> 'is not logged in',
				),
				'logout'		=> array(
					'label'			=> 'Log Out',
					'module'		=> 'default',
					'controller'	=> 'account',
					'action'		=> 'logout',
					'permission'	=> 'is logged in',
				),
			),
		),
		*/
	),
);
