<?php

return array(	
	'method'		=> 'post',
	'elements'		=> array(

        'password'	=> array('password', array(
			'label' => 'New Password',
			'required' => true,
        )),
		
		'submit'		=> array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Reset Password',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);