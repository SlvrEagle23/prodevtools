<?php

return array(	
	'method'		=> 'post',
	'elements'		=> array(
				
		'email'	=> array('text', array(
			'label' => 'E-mail Address',
			'class'	=> 'half-width',
            'validators' => array('EmailAddress'),
			'required' => true,
        )),
		
		'submit'		=> array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Send Password Reset Code',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);