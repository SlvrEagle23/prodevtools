<?php

return array(	
	'method'		=> 'post',
	'elements'		=> array(
				
		'username'	=> array('text', array(
			'label' => 'E-mail Address',
            'validators' => array('EmailAddress'),
			'class'	=> 'half-width',
			'required' => true,
        )),

        'password'	=> array('password', array(
			'label' => 'Password',
			'description' => '<a href="'.\DF\Url::route(array('module' => 'default', 'controller' => 'account', 'action' => 'forgotpw')).'">Forgot your password?</a>',
			'required' => true,
        )),
		
		'submit'		=> array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Log In',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);