<?php

return array(
    'method'		=> 'post',
    'elements'		=> array(

        'name' => array('text', array(
            'label' => 'Your Name',
            'class' => 'half-width',
        )),

        'phone' => array('text', array(
            'label' => 'Phone Number',
            'class' => 'half-width',
            'description' => 'If you wish to be contacted via phone, please specify a phone number here.',
        )),

        'email'	=> array('text', array(
            'label' => 'E-mail Address',
            'description' => 'If you wish to be contacted via e-mail, please specify your e-mail address here.',
            'validators' => array('EmailAddress'),
            'class'	=> 'half-width',
        )),

        'suggestions_materials' => array('textarea', array(
            'label' => 'Suggestions: Materials for Review and Consideration',
            'class' => 'half-width half-height',
        )),

        'suggestions_topics' => array('textarea', array(
            'label' => 'Suggestions: Topics to be Added',
            'class' => 'half-width half-height',
        )),

        'other_feedback' => array('textarea', array(
            'label' => 'Other Comments, Questions or Feedback',
            'class' => 'half-width half-height',
        )),

        'submit'		=> array('submit', array(
            'type'	=> 'submit',
            'label'	=> 'Submit Message',
            'helper' => 'formButton',
            'class' => 'ui-button',
        )),
    ),
);