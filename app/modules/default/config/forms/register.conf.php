<?php

return array(	
	'method'		=> 'post',
	'elements'		=> array(

		'firstname' => array('text', array(
			'label' => 'First Name',
			'class' => 'half-width',
			'required' => true,
		)),

		'lastname' => array('text', array(
			'label' => 'Last Name',
			'class' => 'half-width',
			'required' => true,
		)),

		'phone' => array('text', array(
			'label' => 'Phone Number',
			'class' => 'half-width',
		)),
				
		'email'	=> array('text', array(
			'label' => 'E-mail Address',
			'description' => 'This will also be used as your login for this web site.',
            'validators' => array('EmailAddress'),
			'class'	=> 'half-width',
			'required' => true,
        )),

        'password'	=> array('password', array(
			'label' => 'Password',
			'required' => true,
        )),
		
		'submit'		=> array('submit', array(
			'type'	=> 'submit',
			'label'	=> 'Create Account',
			'helper' => 'formButton',
			'class' => 'ui-button',
		)),
	),
);