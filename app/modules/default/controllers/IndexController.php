<?php
class IndexController extends \DF\Controller\Action
{
    /**
     * Main display.
     */
    public function indexAction()
    {
        $this->view->newest = $this->em->createQuery('SELECT r FROM Entity\Resource r ORDER BY r.created_at DESC')
            ->setMaxResults(8)
            ->getArrayResult();

        $categories_raw = \Entity\ResourceCategory::fetchSelect();
        $categories = array();

        foreach($categories_raw as $cat_id => $cat_name)
        {
            if (stristr($cat_name, ' - ') !== FALSE)
            {
                list($parent, $cat_name) = explode(' - ', $cat_name);
                $categories[$parent][$cat_id] = $cat_name;
            }
            else
            {
                $categories[$cat_id] = $cat_name;
            }
        }

        $this->view->categories = $categories;
    }

    public function aboutAction()
    {

    }

    public function contactAction()
    {
        $form_config = $this->current_module_config->forms->contact->toArray();
        $form = new \DF\Form($form_config);

        if(!empty($_POST) && $form->isValid($_POST))
        {
            $data = $form->getValues();

            \DF\Messenger::send(array(
                'to'        => 'info@professionaldevelopmenttools.com',
                'subject'   => 'Contact Us Form Submission',
                'body'      => $form->populate($data)->renderMessage(),
            ));

            $this->alert('<b>Your message has been sent!</b><br>The Professional Development Tools team will consider any and all feedback, and will promptly answer any questions you included.', 'green');
            $this->redirectHere();
            return;
        }

        if ($this->auth->isLoggedIn())
        {
            $user = $this->auth->getLoggedInUser();
            $user_info = array(
                'name' => $user->firstname . ' ' . $user->lastname,
                'phone' => $user->phone,
                'email' => $user->email,
            );

            $form->setDefaults($user_info);
        }

        $this->view->headTitle('Contact Us');
        $this->renderForm($form);
    }

}