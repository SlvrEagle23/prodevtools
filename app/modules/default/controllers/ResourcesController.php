<?php
use \Entity\Resource;
use \Entity\ResourceType;
use \Entity\ResourceCategory;
use \Entity\ResourceLog;

class ResourcesController extends \DF\Controller\Action
{
    public function indexAction()
    {
        $this->redirectFromHere(array('action' => 'category'));
    }

    public function categoryAction()
    {
        $categories = ResourceCategory::fetchArray();
        $this->view->categories = $categories;

        $cat_id = (int)$this->_getParam('category');

        if ($cat_id != 0)
        {
            $category = ResourceCategory::find($cat_id);

            if (!($category instanceof ResourceCategory))
                $category = NULL;
        }
        else
        {
            $category = NULL;
        }

        $this->view->category = $category;

        if ($category)
        {
            $records = $this->em->createQuery('SELECT r, rt, rc FROM Entity\Resource r JOIN r.type rt LEFT JOIN r.categories rc WHERE rc.id = :cat_id ORDER BY r.title ASC')
                ->setParameter('cat_id', $cat_id);
        }
        else
        {
            $records = $this->em->createQuery('SELECT r, rt, rc FROM Entity\Resource r JOIN r.type rt LEFT JOIN r.categories rc ORDER BY r.title ASC');
        }

        $pager = new \DF\Paginator\Doctrine($records, $this->_getParam('page', 1));
        $this->view->pager = $pager;
    }

    public function searchAction()
    {
        $q = trim($this->_getParam('q', ''));

        if ($q)
        {
            $records = $this->em->createQuery('SELECT r, rt, rc FROM Entity\Resource r JOIN r.type rt LEFT JOIN r.categories rc WHERE r.title LIKE :q OR r.author LIKE :q OR r.description LIKE :q ORDER BY r.title ASC')
                ->setParameter('q', '%'.$q.'%');

            $pager = new \DF\Paginator\Doctrine($records, $this->_getParam('page', 1));
            $this->view->pager = $pager;

            $this->view->q = $q;
        }
    }

    public function viewAction()
    {
        $id = (int)$this->_getParam('id');
        $record = Resource::find($id);

        if (!($record instanceof Resource))
            throw new \DF\Exception\DisplayOnly('Resource not found!');

        $this->view->record = $record;
    }

    public function accessAction()
    {
        $this->acl->checkPermission('is logged in');

        $id = (int)$this->_getParam('id');
        $record = Resource::find($id);

        if (!($record instanceof Resource))
            throw new \DF\Exception\DisplayOnly('Resource not found!');

        $this->view->record = $record;

        $user = $this->auth->getLoggedInUser();

        \Zend_Layout::getMvcInstance()->disableLayout();
        switch(strtolower($this->_getParam('panel', 'frameset')))
        {
            case "track":
                $log = new ResourceLog;
                $log->resource = $record;
                $log->user = $user;
                $log->seconds = 0;
                $log->save();

                $this->view->log = $log;
                $this->render('access_track');
            break;

            case "sync":
                $this->doNotRender();
                $log_id = (int)$this->_getParam('log_id');
                $log = ResourceLog::find($log_id);

                if ($log instanceof ResourceLog && $log->resource_id == $record->id && $log->user_id = $user->id)
                {
                    $log->seconds = $_GET['seconds'];
                    $log->save();

                    echo 'OK';
                }
            break;

            case "frameset":
            default:
                $this->render();
            break;
        }
    }
}