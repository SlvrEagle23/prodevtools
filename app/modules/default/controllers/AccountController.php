<?php
use \Entity\User;

class AccountController extends \DF\Controller\Action
{
	public function indexAction()
	{
		$this->redirectHome();
		return;
	}

	public function loginAction()
    {
    	$form = new \DF\Form($this->current_module_config->forms->login);

    	if ($_POST && $form->isValid($_POST))
    	{
    		$data = $form->getValues();

    		$is_logged_in = $this->auth->authenticate($data);

    		if ($is_logged_in)
    		{
    			$this->alert('<b>Successfully logged in.</b>', 'green');

    			$this->redirectToStoredReferrer('login');
    			return;
    		}
    	}

    	$this->storeReferrer('login');

    	$this->view->headTitle('Log In');
    	$this->renderForm($form);
    }

    public function signupAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->register);

    	if ($_POST && $form->isValid($_POST))
    	{
    		$data = $form->getValues();

    		$existing_user = User::getRepository()->findOneByEmail($data['email']);

    		if ($existing_user instanceof User)
    		{
    			$this->alert('<b>A user already exists with this e-mail address.</b><br>If you forgot your password, enter your e-mail below to have a reset code mailed to you.', 'red');
    			$this->redirectFromHere(array('action' => 'forgotpw'));
    			return;
    		}

    		$user = new User;
    		$user->fromArray($data);
    		$user->save();

    		$this->auth->authenticate($data);

    		$this->alert('<b>You have successfully created a new account.</b><br>Your new account has automatically been logged in.', 'green');
    		$this->redirectHome();
    		return;
    	}

    	$this->view->headTitle('Sign Up');
    	$this->renderForm($form);
	}

	public function forgotpwAction()
	{
		$form = new \DF\Form($this->current_module_config->forms->forgotpw);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			$email = $data['email'];
			$user = User::getRepository()->findOneByEmail($email);

			if ($user instanceof User)
			{
				$user->password_reset_code = substr(strtoupper(md5($user->id.$user->email)), 0, 5);
				$user->save();

				$email_code = base64_encode($user->email.'|'.$user->password_reset_code);
				$email_url = \DF\Url::baseUrl().\DF\Url::route(array('module' => 'default', 'controller' => 'account', 'action' => 'reset', 'code' => $email_code));

				\DF\Messenger::send(array(
					'to'		=> $user->email,
					'subject'	=> 'Password Reset Code',
					'body'		=> 'To reset your password, click the link below:'."\n".$this->view->link($email_url),
					'text_only'	=> TRUE,
				));

				$this->alert('<b>A password reset code has been e-mailed to your address.</b>', 'green');
				$this->redirectHome();
			}
			else
			{
				$this->alert('<b>A user with the e-mail address specified does not exist.</b><br>Please enter another e-mail address below.', 'red');
			}
		}

		$this->view->headTitle('Forgot Password');
		$this->renderForm($form);
	}

	public function resetAction()
	{
		$code_raw = base64_decode($this->_getParam('code', ''));
		list($email, $access) = explode('|', $code_raw);

		$user = User::getRepository()->findOneBy(array(
			'email' 	=> $email,
			'password_reset_code' => $access,
		));

		if (!($user instanceof User))
			throw new \DF\Exception\DisplayOnly('Password reset code invalid.');

		$form = new \DF\Form($this->current_module_config->forms->reset);

		if ($_POST && $form->isValid($_POST))
		{
			$data = $form->getValues();
			$user->setPassword($data['password']);
			$user->password_reset_code = NULL;
			$user->save();

			$this->auth->authenticate(array('email' => $email, 'password' => $data['password']));

			$this->alert('<b>Your password has been reset.</b><br>You have been automatically logged in with your new password.', 'green');
			$this->redirectHome();
			return;
		}

		$this->view->headTitle('Reset Password');
		$this->renderForm($form);
	}

    public function logoutAction()
    {
		$auth = \DF\Auth::logout();
        session_unset();

        $this->redirectHome();
    }
}