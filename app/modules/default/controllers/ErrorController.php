<?php
class ErrorController extends \DF\Controller\Action
{
    public function errorAction()
    {
        // Grab the error object from the request
        $errors = $this->_getParam('error_handler');
        
        // $errors will be an object set as a parameter of the request object, type is a property
        switch ($errors->type)
        {
            case \Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case \Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                $this->_helper->viewRenderer('error/pagenotfound', null, true);
				
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404) ;
                $this->view->message = 'Page not found' ;
            break;
                
            default:
				if ($errors->exception instanceof \DF\Exception\DisplayOnly)
				{
					$this->_helper->viewRenderer('error/displayonly', NULL, TRUE);
				}
				else if ($errors->exception instanceof \DF\Exception\NotLoggedIn)
				{
					$this->_helper->viewRenderer('error/notloggedin', NULL, TRUE);
                    $this->view->message = 'Login Required to Access This Page';
				}
				else if ($errors->exception instanceof \DF\Exception\PermissionDenied)
				{
					$this->_helper->viewRenderer('error/accessdenied', NULL, TRUE);
                    $this->view->message = 'Access Denied';
				}
            	else
            	{
            		$this->_helper->viewRenderer('error/error', null, true);
					
                    // Application Error
                    // $this->getResponse()->setHttpResponseCode(500);
                    $this->view->message = 'Application error';
                    
                    // Send notification.
                    if ($errors->exception)
                    {
                        try
                        {
                            $exception = $errors->exception;
                            $exception_data = array(
                                'exception'		=> nl2br((string)$exception),
                                'file'			=> $exception->getFile(),
                                'line'			=> $exception->getLine(),
                                'trace'			=> nl2br($exception->getTraceAsString()),
                                'message'		=> $exception->getMessage(),
                                'extra_message' => (isset($options['message'])) ? $options['message'] : '',
                                'ip'			=> $_SERVER['REMOTE_ADDR'],
                                'browser'		=> $_SERVER['HTTP_USER_AGENT'],
                                'uri'			=> $_SERVER['REQUEST_URI'],
                                'referrer'		=> $_SERVER['HTTP_REFERER'],
                                'variables'		=> print_r($_REQUEST, TRUE),
                            );
                            
                            \DF\Messenger::send(array(
                                'to'		=> $this->config->application->mail->error_addr,
                                'subject'	=> 'Critical System Error',
                                'module'    => 'default',
                                'template'	=> 'error',
                                'vars'      => array('exception_data' => $exception_data),
                            ));
                        }
                        catch(\Exception $e)
                        {}
                    }
            	}
            break; 
        }
		
        // pass the actual exception object to the view
        $this->view->exception = $errors->exception;
		
        // pass the request to the view
        $this->view->request = $errors->request;
    }
}