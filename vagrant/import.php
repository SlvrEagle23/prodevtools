<?php
/**
 * Vagrant Remote importer script.
 */

$tables_to_migrate = array(
    // Common tables to applications.
    'settings',
    'action',
    'block',
    'role',
    'role_has_action',
    'user',
    'user_has_role',

    // Specific to this application.
    'resource_type',
    'resource_category',
    'resource',
    'resource_has_category',
    'resource_logs',
);

require_once dirname(__FILE__) . '/../app/bootstrap.php';
$application->bootstrap();

set_time_limit(0);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);

if (!($config->db_prod instanceof \Zend_Config))
    die('No production database configuration specified!');

$em = \Zend_Registry::get('em');
$local = $em->getConnection();

$remote_config = new \Doctrine\DBAL\Configuration;
$remote_params = $config->db_prod->toArray();
$remote = \Doctrine\DBAL\DriverManager::getConnection($remote_params, $remote_config);

foreach($tables_to_migrate as $table_name)
{
    $stmt = $remote->query('SELECT * FROM '.$remote->quoteIdentifier($table_name));

    while ($row = $stmt->fetch())
    {
        $local->insert($table_name, $row);
    }

    echo "Imported: $table_name\n";
}

echo "DB import complete!";
exit;