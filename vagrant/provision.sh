#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
export app_base=/var/www
export tmp_base=$app_base/tmp
export www_base=$app_base/web

if [ ! -f $app_base/.deploy_run ]
then

    # Trigger mlocate reindex.
    sudo updatedb

    # Set up environment.
    touch $app_base/app/.updated

    echo 'development' > $app_base/app/.env

    # Permission grouping.
    usermod -G vagrant www-data
    usermod -G vagrant nobody
    usermod -G www-data vagrant

    # Enable PHP flags.
    echo "alias phpwww='sudo -u vagrant php'" >> /home/vagrant/.profile

    # Install composer.
    echo "Installing Composer..."
    cd /root
    curl -sS https://getcomposer.org/installer | php
    mv composer.phar /usr/local/bin/composer

    # Mark deployment as run.
    touch $app_base/.deploy_run

else

    echo 'DROP DATABASE app;' | mysql -u root
    echo 'CREATE DATABASE app CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;' | mysql -u root
    service mysql restart

fi

# Copy sample files.
if [ ! -f $app_base/app/config/db.conf.php ]
then
	cp $app_base/app/config/db.conf.sample.php $app_base/app/config/db.conf.php
fi

# Run Composer.js
if [ ! -f $app_base/vendor/autoload.php ]
then
	cd $app_base
	composer install
fi

# Set up DB.
echo "Setting up database..."

cd $app_base
sudo -u vagrant php doctrine.php orm:schema-tool:create

sudo -u vagrant php vagrant/import.php

echo "One-time setup complete!"