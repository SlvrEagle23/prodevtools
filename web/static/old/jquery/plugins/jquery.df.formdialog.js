(function($) {
    /* DoIT Framework FormDialog plugin */
    $.fn.DFFormDialog = function(form_name, options) {
        var config = {
            autoOpen: false,
            closeOnEscape: true,
            bgiframe: false,
            resizable: true,
            modal: true,
            overlay: {
                backgroundColor: '#000',
                opacity: 0.75
            },
            width: 500,
            height: 425,
            title: '',
            close: function(){
                $('input[name*="_date"]').datepicker('hide');
            }
        };

        if (options) $.extend(config, options);

        return this.each(function() {
            if( !$(this).is('a') )
                throw new Error("DFFormDialog can only be applied to an anchor element, just attempted to apply to a " + this.tagName);

            $(this).click(function(e) {
                e.preventDefault();
                openFormDialog(form_name, $(this).attr('href'), config);
            });
        });
    };
    
    var openFormDialog = function(form_name,a_href,config) {

        var div_id = '#'+form_name+'_form_dialog';

        if ($(div_id).length == 0)
        {
            $('body').append('<div id="'+div_id.substring(1)+'"><span class="loading_msg"></span></div>');
            $(div_id).dialog(config);
        }

        if ($(div_id+' form').length > 0)
        {
            $(div_id).dialog('open');
        }
        else
        {
            $(div_id+' .loading_msg').addClass('loading').html('loading form...');
            $(div_id).dialog('open');
            $.get(
                a_href + '/ajax/true',
                function(data, textStatus)
                {
                    if (textStatus == 'success')
                    {
                        $(div_id+' .loading_msg').removeClass('loading').html('');
                        $(div_id).append(data);
                        
                        /* Add a cancel button to the form. */
                        var link_element = $('<a />')
                            .attr('href', '#')
                            .attr('style', 'margin-right: 10px;')
                            .text('Cancel')
                            .addClass('ui-button')
                            .click(function() {
                                $(div_id).dialog('close');
                            });
                        
                        $(div_id).find('#submit-element').prepend(link_element);
                        
                        initPage();
                    }
                    else
                    {
                        $(div_id+' .loading_msg').removeClass('loading').html('Error loading form');
                    }
                });
        }
        return false;
    }

})(jQuery);