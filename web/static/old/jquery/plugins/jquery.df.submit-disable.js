(function($) {
    $.fn.DisableSubmitButtons = function() {
        $(this).each(function(){
            
            if( !$(this).is('form') )
                throw "Element not a form";
            
            // On submit disable its submit button
            $(this).find(':submit:not(:hidden)').each(function(){
                //We swap the buttons out so that the default behavior of sending
                //button value on click will still work.
                
                var originalButton = this;
                
                $(originalButton)
                    .addClass('caseman-disabled-original')
                    .attr('caseman-disabled', true);
                
                var newButton = $(this).clone();

                $(originalButton).hide();
                
                $(newButton).attr('disabled', 'disabled')
                       .addClass('disabled')
                       .addClass('caseman-disabled-auto')
                       .removeAttr('name')
                       .removeAttr('clicked')
                       .unbind('click')
                       .val('Working...')
                       .insertAfter(originalButton);
                       
            });
        });
    };
    
    $.fn.EnableSubmitButtons = function() {
        $(this).each(function(){
            
            if( !$(this).is('form') )
                throw "Element not a form";
            
            // On reset remove all non-hidden buttons
            $(this).find(':submit.caseman-disabled-auto').remove();
            
            // show hidden buttons
            $(this).find(':submit:hidden.caseman-disabled-original').each(function(){
                $(this)
                    .removeClass('disabled')
                    .removeClass('caseman-disabled-original')
                    .removeAttr('disabled')
                    .removeAttr('caseman-disabled')
                    .show();
            });
        });
    };

})(jQuery);