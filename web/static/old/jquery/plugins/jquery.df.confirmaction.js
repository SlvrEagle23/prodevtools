(function($) {
    /* DF ConfirmDelete plugin */
    $.fn.DFConfirmAction = function() {
        return this.each(function() {

            if(!$(this).is('a'))
                throw new Error("ConfirmAction cannot apply to a " + this.tagName);

            $(this).click(function(e) {
                e.preventDefault();
                var thishref = $(this).attr('href');
                var thistitle = $(this).attr('title');
                if (thistitle)
                    var message = 'Are you sure you want to '+thistitle+'?';
                else
                    var message = 'Are you sure you want to complete this action?';
                confirmAction('Confirm Action', message,thishref);
            });
        });
    };

    var confirmAction = function(title, message, actionurl)
    {
        var dialog = $('<div id="confirm-dialog" title="' + title + '">'
            + '<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>' + message + '</p><div class="clear">&nbsp;</div>'
            + '<div class="buttons"></div></div>').dialog({
                    bgiframe: true,
                    resizable: false,
                    height: 180,
                    width: 350,
                    modal: true,
                    close: function() {
                        $(dialog).remove();
                    }
                });
        var cancelButton = $('<a class="button" href="">Cancel</a>')
            .button()
            .click(function(e) {
                e.preventDefault();
                $(dialog).dialog('close');
                return false;
            })
            .appendTo('#confirm-dialog .buttons', dialog);
        var confirmButton = $('<a class="button" style="margin-left: 10px;" href="">OK</a>')
            .button()
            .click(function(e) {
                e.preventDefault();
                $(this).button({
                    'disabled': true,
                    'label': 'Working...'
                });
                location.href = actionurl;
                return false;
            })
            .appendTo('#confirm-dialog .buttons', dialog);
    };

})(jQuery);